/* SPDX-License-Identifier: MIT */
/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/* A simple tool that sets up the org.freedesktop.portal.EmulatedInput DBus
 * interface and provides the required functionality.
 *
 * This tool is useful for testing the libei portal backend, it provides just
 * enough data to have that backend succeed with the connection.
 *
 * This tool does not run an EIS server, use e.g. the eis-demo-server.
 *
 * Usually, you'd want to:
 * - run the eis-demo-server (or some other EIS implementation)
 * - run the eis-fake-portal
 * - run the libei client relying on the portal
 */

#include "config.h"

#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdbool.h>
#include <unistd.h>

#include "util-io.h"
#include "util-list.h"
#include "util-mem.h"
#include "util-logger.h"
#include "util-strings.h"

#include "libreis.h"

#include <systemd/sd-bus.h>

DEFINE_UNREF_CLEANUP_FUNC(reis);

struct session {
	struct list link;
	struct sd_bus_slot *slot;
	char *handle;
};

struct portal {
	struct logger *logger;
	char *busname;

	struct list sessions;
} portal;


#define call(_call) do { \
	int _rc = _call; \
	if (_rc < 0) { \
		log_error(portal, "Failed with %s %s:%d\n", strerror(-_rc), __func__, __LINE__); \
		return _rc; \
	} } while(0)

static int
session_close(sd_bus_message *m, void *userdata, sd_bus_error *ret_error)
{
	/* Not implemented */
	return 0;
}

static const sd_bus_vtable session_vtable[] = {
	SD_BUS_VTABLE_START(0),
	SD_BUS_METHOD("Close", "", "", session_close, SD_BUS_VTABLE_UNPRIVILEGED),
	SD_BUS_SIGNAL("Closed", "ua{sv}", 0),
	SD_BUS_VTABLE_END,
};

static int
create_session_object(struct portal *portal,
		      sd_bus *bus,
		      const char *objectpath)
{
	struct session *session = calloc(sizeof *session, 1);

	session->handle = xstrdup(objectpath);

	call(sd_bus_add_object_vtable(bus, &session->slot,
				      objectpath,
				      "org.freedesktop.portal.Session",
				      session_vtable,
				      NULL));

	list_append(&portal->sessions, &session->link);

	log_debug(portal, "Session created on %s\n", objectpath);
	return 0;
}

static int
request_close(sd_bus_message *m, void *userdata, sd_bus_error *ret_error)
{
	/* We don't live long enough for this to be called */
	return 0;
}

static const sd_bus_vtable request_vtable[] = {
	SD_BUS_VTABLE_START(0),
	SD_BUS_METHOD("Close", "", "", request_close, SD_BUS_VTABLE_UNPRIVILEGED),
	SD_BUS_SIGNAL("Response", "ua{sv}", 0),
	SD_BUS_VTABLE_END,
};

static int
create_request_object(struct portal *portal,
		      sd_bus *bus,
		      const char *objectpath,
		      const char *session_path)
{
	_unref_(sd_bus_slot) *slot = NULL;

	create_session_object(portal, bus, session_path);

	call(sd_bus_add_object_vtable(bus, &slot,
				      objectpath,
				      "org.freedesktop.portal.Request",
				      request_vtable,
				      NULL));

	int response = 0;
	log_debug(portal, "emitting Response %d on %s\n", response, objectpath);
	return sd_bus_emit_signal(bus,
			   objectpath,
			   "org.freedesktop.portal.Request",
			   "Response",
			   "ua{sv}",
			   response,
			   1,
			   "session_handle",/* string key */
			   "s", session_path /* variant string */
			  );
	/* note: _unref_ removes object immediately */
}

static char *
sender_token(const char *input)
{
	if (!input)
		return NULL;

	char *token = strstrip(input, ":");
	for (size_t idx = 0; token[idx]; idx++) {
		if (token[idx] == '.')
			token[idx] = '_';
	}

	return token;
}

static int
portal_create_session(sd_bus_message *m, void *userdata,
		      sd_bus_error *ret_error)
{
	struct portal *portal = userdata;

	call(sd_bus_message_enter_container(m, 'a', "{sv}"));

	const char *session_token = NULL;
	const char *handle_token = NULL;

	const char *key, *val;
	call(sd_bus_message_read(m, "{sv}", &key, "s", &val));
	if (streq(key, "handle_token"))
		handle_token = val;
	else if (streq(key, "session_handle_token"))
		session_token = val;
	else
		return -EINVAL;

	call(sd_bus_message_read(m, "{sv}", &key, "s", &val));
	if (streq(key, "handle_token"))
		handle_token = val;
	else if (streq(key, "session_handle_token"))
		session_token = val;
	else
		return -EINVAL;

	assert(handle_token);
	assert(session_token);

	call(sd_bus_message_exit_container(m));

	_cleanup_free_ char *sender = sender_token(sd_bus_message_get_sender(m));
	if (!sender)
		return -ENOMEM;

	/* Send back the object path of the object we're about to create. We
	 * then create the object, so if that fails we have a problem but
	 * meh, this is for testing only .*/
	_cleanup_free_ char *objpath = xaprintf("%s/request/%s/%s",
						"/org/freedesktop/portal/desktop",
						sender,
						handle_token);
	call(sd_bus_reply_method_return(m, "o", objpath));

	_cleanup_free_ char *session_path = xaprintf("%s/session/%s/%s",
						     "/org/freedesktop/portal/desktop",
						     sender,
						     session_token);

	/* now create the object */
	return create_request_object(portal, sd_bus_message_get_bus(m), objpath, session_path);
}

static void
set_prop_cmdline(struct reis *reis)
{
	_cleanup_free_ char *cmdline = cmdline_as_str();
	reis_set_property_with_permissions(reis, "ei.application.cmdline", cmdline, REIS_PROPERTY_PERM_NONE);
}

static void
set_prop_pid(struct reis *reis)
{
	char pid[64];

	xsnprintf(pid, sizeof(pid), "%i", getpid());
	reis_set_property_with_permissions(reis, "ei.application.pid", pid, REIS_PROPERTY_PERM_NONE);
}

static void
set_prop_type(struct reis *reis)
{
	reis_set_property_with_permissions(reis, "ei.connection.type", "portal", REIS_PROPERTY_PERM_NONE);
}

static int
portal_connect_to_eis(sd_bus_message *m, void *userdata, sd_bus_error *ret_error)
{
	struct portal *portal = userdata;

	const char *xdg = getenv("XDG_RUNTIME_DIR");
	if (!xdg)
		return -ENOENT;

	const char *session_handle;

	call(sd_bus_message_read(m, "oa{sv}", &session_handle, 0));

	struct session *session;
	bool found = false;
	list_for_each_safe(session, &portal->sessions, link) {
		if (streq(session->handle, session_handle)) {
			found = true;
			break;
		}
	}

	/* We're aborting here because it's a client bug and this is just a
	 * fake portal */
	if (!found) {
		log_error(portal, "Invalid session handle %s\n", session_handle);
		return sd_bus_reply_method_return(m, "ua{sv}", 1, 0);
	}

	log_info(portal, "Valid session %s, connecting to EIS\n", session_handle);

	_cleanup_free_ char *sockpath = xstrdup(getenv("LIBEI_SOCKET"));
	if (!sockpath)
		sockpath = xaprintf("%s/eis-0", xdg);

	int handle = xconnect(sockpath);
	if (handle < 0) {
		log_error(portal, "Failed to connect to EIS (%s)\n", strerror(-handle));
		return sd_bus_reply_method_return(m, "ua{sv}", 1, 0);
	}

	_unref_(reis) *reis = reis_new(handle);
	assert(reis);
	set_prop_pid(reis);
	set_prop_cmdline(reis);
	set_prop_type(reis);

	log_debug(portal, "passing Handle %d\n", handle);
	return sd_bus_reply_method_return(m, "ua{sv}", 0,
					  1, /* array size */
					  "fd", "h", handle);
}

static const sd_bus_vtable portal_vtable[] = {
	SD_BUS_VTABLE_START(0),
	SD_BUS_METHOD("CreateSession", "a{sv}", "o", portal_create_session, SD_BUS_VTABLE_UNPRIVILEGED),
	SD_BUS_METHOD("ConnectToEIS", "oa{sv}", "ua{sv}", portal_connect_to_eis, SD_BUS_VTABLE_UNPRIVILEGED),
	SD_BUS_VTABLE_END,
};

static int
run(struct portal *portal)
{
	_unref_(sd_bus) *bus = NULL;
	_unref_(sd_bus_slot) *slot = NULL;
	int rc = sd_bus_open_user(&bus);
	if (rc < 0)
		return rc;

	rc = sd_bus_add_object_vtable(bus, &slot,
				      "/org/freedesktop/portal/desktop",
				      "org.freedesktop.portal.EmulatedInput",
				      portal_vtable,
				      portal);
	if (rc < 0)
		return rc;

	log_debug(portal, "Portal object at: /org/freedesktop/portal/desktop\n");

	rc = sd_bus_request_name(bus, portal->busname, 0);
	if (rc < 0)
		return rc;

	log_debug(portal, "Portal DBus name: %s\n", portal->busname);

	_unref_(sd_event) *event = NULL;
	rc = sd_event_default(&event);
	if (rc < 0)
		return rc;

	rc = sd_event_set_watchdog(event, true);
	if (rc < 0)
		return rc;

	rc = sd_bus_attach_event(bus, event, 0);
	if (rc < 0)
		return rc;

	return sd_event_loop(event);
}

static void
usage(FILE *fp, const char *argv0)
{
	fprintf(fp,
		"Usage: %s [--busname=a.b.c.d]\n"
		"\n"
		"Emulates an XDG Desktop portal for the org.freedesktop.portal.EmulatedInput interface\n"
		"\n"
		"Options:\n"
		" --busname	    use the given busname instead of the default org.freedesktop.portal.Desktop\n"
		"\n"
		"This portal connects directly to the EIS instance at $LIBEI_SOCKET or, if unset, at \n"
		"$XDG_RUNTIME_DIR/eis-0. It does **not** use the org.freedesktop.impl.portal.EmulatedInput\n"
		"interface.\n"
		"",
		basename(argv0));
}

int
main(int argc, char **argv)
{
	_cleanup_free_ char *busname = xstrdup("org.freedesktop.portal.Desktop");

	while (1) {
		enum opts {
			OPT_BUSNAME,
		};
		static struct option long_opts[] = {
			{ "busname",	required_argument, 0, OPT_BUSNAME},
			{ "help",	no_argument, 0, 'h'},
			{ .name = NULL },
		};

		int optind = 0;
		int c = getopt_long(argc, argv, "h", long_opts, &optind);
		if (c == -1)
			break;

		switch(c) {
			case 'h':
				usage(stdout, argv[0]);
				return EXIT_SUCCESS;
			case OPT_BUSNAME:
				free(busname);
				busname = xstrdup(optarg);
				break;
			default:
				usage(stderr, argv[0]);
				return EXIT_FAILURE;
		}
	}

	list_init(&portal.sessions);
	portal.busname = steal(&busname);
	portal.logger = logger_new("portal", NULL);
	logger_set_priority(portal.logger, LOGGER_DEBUG);

	int rc = run(&portal);
	if (rc < 0)
		fprintf(stderr, "Failed to start fake portal: %s\n", strerror(-rc));

	logger_unref(portal.logger);
	free(portal.busname);
	return rc == 0;
}
