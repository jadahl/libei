/* SPDX-License-Identifier: MIT */
/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "config.h"


#include "libei-private.h"

static void
ei_region_destroy(struct ei_region *region)
{
	list_remove(&region->link);
}

_public_
OBJECT_IMPLEMENT_REF(ei_region);
_public_
OBJECT_IMPLEMENT_UNREF_CLEANUP(ei_region);
static
OBJECT_IMPLEMENT_CREATE(ei_region);
_public_
OBJECT_IMPLEMENT_GETTER(ei_region, user_data, void *);
_public_
OBJECT_IMPLEMENT_SETTER(ei_region, user_data, void *);
_public_
OBJECT_IMPLEMENT_GETTER(ei_region, x, uint32_t);
_public_
OBJECT_IMPLEMENT_GETTER(ei_region, y, uint32_t);
_public_
OBJECT_IMPLEMENT_GETTER(ei_region, width, uint32_t);
_public_
OBJECT_IMPLEMENT_GETTER(ei_region, height, uint32_t);
_public_
OBJECT_IMPLEMENT_GETTER(ei_region, physical_scale, double);
OBJECT_IMPLEMENT_SETTER(ei_region, physical_scale, double);

struct ei_region *
ei_region_new(void)
{
	struct ei_region *region = ei_region_create(NULL);

	region->physical_scale = 1.0;
	list_init(&region->link);

	return region;
}

void
ei_region_set_offset(struct ei_region *region, uint32_t x, uint32_t y)
{
	region->x = x;
	region->y = y;
}

void
ei_region_set_size(struct ei_region *region, uint32_t w, uint32_t h)
{
	region->width = w;
	region->height = h;
}

_public_ bool
ei_region_contains(struct ei_region *r, double x, double y)
{
	return (x >= r->x && x < r->x + r->width &&
		y >= r->y && y < r->y + r->height);
}

_public_ bool
ei_region_convert_point(struct ei_region *r, double *x, double *y)
{
	if (ei_region_contains(r, *x, *y)) {
		*x -= r->x;
		*y -= r->y;
		return true;
	}

	return false;
}
