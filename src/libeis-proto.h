/* SPDX-License-Identifier: MIT */
/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include "config.h"

#include <stddef.h>

#include "util-macros.h"
#include "util-mem.h"

#include "brei-shared.h"
#include "libeis-private.h"

/* callbacks invoked during eis_proto_parse_message() */
struct eis_proto_interface {
	int (*connect)(struct eis_client *client, const char *name, bool is_sender);
	int (*connect_done)(struct eis_client *client);
	int (*disconnect)(struct eis_client *client);
	int (*bind_seat)(struct eis_client *client, uint32_t seatid, enum eis_device_capability cap);
	int (*unbind_seat)(struct eis_client *client, uint32_t seatid, enum eis_device_capability cap);
	int (*close_device)(struct eis_client *client, uint32_t deviceid);
	int (*configure_name)(struct eis_client *client, const char *name);
	int (*configure_capabilities)(struct eis_client *client, uint32_t allow);
	int (*property)(struct eis_client *client, const char *name,
			const char *value, uint32_t permissions);
	/* events */
	int (*start_emulating)(struct eis_client *client, uint32_t deviceid);
	int (*stop_emulating)(struct eis_client *client, uint32_t deviceid);
	int (*rel)(struct eis_client *client, uint32_t deviceid, double x, double y);
	int (*abs)(struct eis_client *client, uint32_t deviceid, double x, double y);
	int (*button)(struct eis_client *client, uint32_t deviceid, uint32_t button, bool is_press);
	int (*key)(struct eis_client *client, uint32_t deviceid, uint32_t key, bool is_press);
	int (*scroll)(struct eis_client *client, uint32_t deviceid, double x, double y);
	int (*scroll_stop)(struct eis_client *client, uint32_t deviceid, bool x, bool y, bool is_cancel);
	int (*scroll_discrete)(struct eis_client *client, uint32_t deviceid, int32_t x, int32_t y);
	int (*touch_down)(struct eis_client *client, uint32_t deviceid,
			 uint32_t tid, double x, double y);
	int (*touch_motion)(struct eis_client *client, uint32_t deviceid,
			    uint32_t tid, double x, double y);
	int (*touch_up)(struct eis_client *client, uint32_t deviceid, uint32_t tid);
	int (*frame) (struct eis_client *client, uint32_t deviceid);
};

struct eis_proto_requests {
	int (*connected)(struct eis_client *client);
	int (*disconnected)(struct eis_client *client);
	int (*seat_added)(struct eis_seat *seat);
	int (*seat_removed)(struct eis_seat *seat);
	int (*device_added)(struct eis_device *device);
	int (*device_removed)(struct eis_device *device);
	int (*device_paused)(struct eis_device *device);
	int (*device_resumed)(struct eis_device *device);
	int (*device_keymap)(struct eis_device *device);
	int (*device_done)(struct eis_device *device);
	int (*device_region)(struct eis_device *device,
			     const struct eis_region *region);
	int (*keyboard_modifiers)(struct eis_device *device,
				  const struct eis_xkb_modifiers *mods);
	int (*property)(struct eis_client *client, const char *name,
			const char *value, uint32_t permissions);

	/* events */
	int (*start_emulating)(struct eis_device *device, uint32_t deviceid);
	int (*stop_emulating)(struct eis_device *device, uint32_t deviceid);
	int (*rel)(struct eis_device *device, uint32_t deviceid, double x, double y);
	int (*abs)(struct eis_device *device, uint32_t deviceid, double x, double y);
	int (*button)(struct eis_device *device, uint32_t deviceid, uint32_t button, bool is_press);
	int (*key)(struct eis_device *device, uint32_t deviceid, uint32_t key, bool is_press);
	int (*scroll)(struct eis_device *device, uint32_t deviceid, double x, double y);
	int (*scroll_stop)(struct eis_device *device, uint32_t deviceid, bool x, bool y, bool is_cancel);
	int (*scroll_discrete)(struct eis_device *device, uint32_t deviceid, int32_t x, int32_t y);
	int (*touch_down)(struct eis_device *device, uint32_t deviceid,
			 uint32_t tid, double x, double y);
	int (*touch_motion)(struct eis_device *device, uint32_t deviceid,
			    uint32_t tid, double x, double y);
	int (*touch_up)(struct eis_device *device, uint32_t deviceid, uint32_t tid);
	int (*frame) (struct eis_device *device, uint32_t deviceid);
};

int
eis_proto_handle_message(struct eis_client *client,
			 const struct eis_proto_interface *interface,
			 struct brei_message *message);

const struct eis_proto_requests *
eis_proto_get_requests(void);
