/* SPDX-License-Identifier: MIT */
/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <stdint.h>

#define _sentinel_ __attribute__((sentinel))

/**
 * REIS is the library for Restrictions for EIS. This library is used by
 * intermediaries between EI and EIS to reduce the capabilities that an EI
 * client has available.
 *
 * It is a helper library that does not initiate a full EI or EIS
 * context but works on the file descriptor instead.
 *
 * Restricting capabilities is a one-way-road. A default EIS context has
 * full permissions, consecutive calls can only restrict the current
 * permissions but not loosen them.
 */

struct reis;

enum reis_device_capability {
	REIS_DEVICE_CAP_POINTER = 1,
	REIS_DEVICE_CAP_POINTER_ABSOLUTE,
	REIS_DEVICE_CAP_KEYBOARD,
	REIS_DEVICE_CAP_TOUCH,

	REIS_DEVICE_CAP_ALL = ~0,
};

/**
 * @enum reis_property_permission
 *
 * A set of masks for operations permitted on properties. Note that property
 * permissions only affect the libreis client, the server has full access to the
 * properties at any time.
 */
enum reis_property_permission {
	REIS_PROPERTY_PERM_NONE = 0,
	REIS_PROPERTY_PERM_READ = (1 << 0),
	REIS_PROPERTY_PERM_WRITE = (1 << 1),
	REIS_PROPERTY_PERM_DELETE = (1 << 2),

	REIS_PROPERTY_PERM_ALL = (REIS_PROPERTY_PERM_READ|REIS_PROPERTY_PERM_WRITE|REIS_PROPERTY_PERM_DELETE),
};

/**
 * Create a new reis context based on the EIS connection at the other end of
 * @a eisfd. The EIS context does not need to be in any
 * specific state and no checking is done that there is indeed an EIS
 * context at the other end of the fd.
 */
struct reis *
reis_new(int eisfd);

struct reis *
reis_unref(struct reis* reis);

/**
 * See ei_property_set_with_permissions(), but the permissions are
 * left as-is. If the property does not exist, it is created with permissions
 * @ref REIS_PROPERTY_PERM_ALL.
 */
int
reis_set_property_with_permissions(struct reis *reis,
				   const char *property, const char *value,
				   uint32_t permission);

/**
 * Set the name for the client on this connection.
 *
 * This function has no effect if the EI client has already sent the
 * connection message to the server. IOW this can only be used to *set* the
 * name but not to *change* the name of the client.
 *
 * Calling this function multiple times has no effect, only the first name
 * is used.
 *
 * @return zero on success or a negative errno otherwise
 */
int
reis_set_name(struct reis *reis, const char *name);

/**
 * Explicitly allow the given capabilities. The argument list must be
 * terminated with zero.
 *
 * By default, an EIS implementation will allow any capability. Calling this
 * function changes the EIS implementation's default behavior to deny all
 * capabilities EXCEPT the ones given in this call. For example, the following
 * code only allows the pointer and keyboard capabilty:
 *
 * @code
 * reis_allow_capability(reis, REIS_DEVICE_CAP_POINTER,
 *                       REIS_DEVICE_CAP_KEYBOARD, 0);
 * @endcode
 *
 * @return Zero on success or a negative errno on failure
 */
_sentinel_ int
reis_allow_capability(struct reis *reis, enum reis_device_capability cap, ...);
