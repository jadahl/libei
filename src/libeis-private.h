/* SPDX-License-Identifier: MIT */
/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <stdarg.h>

#include "util-object.h"

#include "libeis.h"
#include "util-macros.h"
#include "util-list.h"
#include "util-sources.h"
#include "util-structs.h"

struct eis_backend_interface {
	void (*destroy)(struct eis *eis, void *backend);
};

struct eis {
	struct object object;
	void *user_data;
	struct sink *sink;
	struct list clients;

	struct eis_backend_interface backend_interface;
	void *backend;
	struct list event_queue;

	struct {
		eis_log_handler handler;
		enum eis_log_priority priority;
	} log;

	const struct eis_proto_requests *requests;
};

enum eis_client_state {
	EIS_CLIENT_STATE_NEW,		/* just connected */
	EIS_CLIENT_STATE_CONNECTING,	/* client requested connect */
	EIS_CLIENT_STATE_CONNECTED,	/* server has sent connect */
	EIS_CLIENT_STATE_DISCONNECTED,
};

struct eis_client {
	struct object object;
	void *user_data;
	struct list link;
	struct source *source;
	uint32_t id;
	enum eis_client_state state;
	char *name;
	bool is_sender;

	struct list seats;
	struct list seats_pending;

	struct list properties;

	struct {
		uint32_t cap_allow_mask;
	} restrictions;
};

enum eis_seat_state {
	EIS_SEAT_STATE_PENDING,
	EIS_SEAT_STATE_ADDED,
	EIS_SEAT_STATE_BOUND,
	EIS_SEAT_STATE_REMOVED_INTERNALLY, /* Removed internally but eis_seat_remove() may be called */
	EIS_SEAT_STATE_REMOVED, /* Removed but still waiting for some devices to be removed */
	EIS_SEAT_STATE_DEAD, /* Removed from our list */
};

struct eis_seat {
	struct object object; /* parent is ei_client */
	struct list link;
	void *user_data;
	uint32_t id;

	enum eis_seat_state state;
	char *name;
	uint32_t capabilities_mask;

	struct list devices;
};

enum eis_device_state {
	EIS_DEVICE_STATE_NEW,
	EIS_DEVICE_STATE_PAUSED,
	EIS_DEVICE_STATE_RESUMED,
	EIS_DEVICE_STATE_EMULATING,
	EIS_DEVICE_STATE_CLOSED_BY_CLIENT,
	EIS_DEVICE_STATE_DEAD,
};

struct eis_region {
	struct object object;
	struct eis_device *device;
	void *user_data;
	bool added_to_device;
	struct list link;
	uint32_t x, y;
	uint32_t width, height;
	double physical_scale;
};

struct eis_device {
	struct object object; /* parent is ei_seat, and we have a ref to it */
	struct list link;
	uint32_t id;
	char *name;
	enum eis_device_state state;
	uint32_t capabilities;
	void *user_data;
	enum eis_device_type type;

	uint32_t width, height;

	struct list regions;
	struct list regions_new; /* not yet added */

	struct eis_keymap *keymap;

	struct {
		bool x_is_stopped, y_is_stopped;
		bool x_is_cancelled, y_is_cancelled;
	} scroll;

};

struct eis_touch {
	struct object object;
	struct eis_device *device;
	void *user_data;
	uint32_t tracking_id;
	enum {
		TOUCH_IS_NEW,
		TOUCH_IS_DOWN,
		TOUCH_IS_UP,
	} state;

	double x, y;
};

struct eis_event {
	struct object object;
	enum eis_event_type type;
	struct list link;
	struct eis_client *client;
	struct eis_seat *seat;
	struct eis_device *device;

	union {
		struct {
			uint32_t capabilities;
		} bind;
		struct {
			double dx, dy; /* relative motion */
			double absx, absy; /* absolute motion */
			double sx, sy; /* scroll */
			int32_t sdx, sdy; /* discrete scroll */
			bool stop_x, stop_y; /* scroll stop */
			uint32_t button;
			bool button_is_press;
		} pointer;
		struct {
			uint32_t key;
			bool key_is_press;
		} keyboard;
		struct {
			uint32_t touchid;
			double x, y;
		} touch;
		struct {
			char *name;
			char *value;
			uint32_t permissions;
		} prop;
	};
};

struct eis_keymap {
	struct object object;
	struct eis_device *device;
	void *user_data;
	enum eis_keymap_type type;
	int fd;
	size_t size;
	bool assigned;
};

struct eis_xkb_modifiers {
	uint32_t depressed;
	uint32_t locked;
	uint32_t latched;
	uint32_t group;
};

struct eis_property {
	struct object object;
	struct list link;
	char *name;
	char *value;
	uint32_t permissions;
};

struct eis_property *
eis_property_unref(struct eis_property *prop);

void
eis_property_update_from_client(struct eis_client *client, const char *name,
				const char *value, uint32_t permissions);

void
eis_init_object(struct eis *eis, struct object *parent);

int
eis_init(struct eis *eis);

struct eis_client *
eis_client_new(struct eis *eis, int fd);

struct eis*
eis_client_get_context(struct eis_client *client);

void
eis_add_client(struct eis *eis, struct eis_client *client);

void
eis_client_add_seat(struct eis_client *client, struct eis_seat *seat);
void
eis_client_remove_seat(struct eis_client *client, struct eis_seat *seat);

void
eis_client_add_device(struct eis_client *client, struct eis_device *device);
void
eis_client_remove_device(struct eis_client *client, struct eis_device *device);

void
eis_client_resume_device(struct eis_client *client,
			 struct eis_device *device);
void
eis_client_pause_device(struct eis_client *client,
			  struct eis_device *device);

void
eis_client_keyboard_modifiers(struct eis_client *client, struct eis_device *device,
			      uint32_t depressed, uint32_t latched, uint32_t locked,
			      uint32_t group);

void
eis_seat_bind(struct eis_seat *seat, uint32_t cap);

void
eis_seat_drop(struct eis_seat *seat);

struct eis *
eis_device_get_context(struct eis_device *device);

void
eis_device_set_client_keymap(struct eis_device *device,
		      enum eis_keymap_type type,
		      int keymap_fd, size_t size);
int
eis_device_event_frame(struct eis_device *device);

int
eis_device_event_pointer_rel(struct eis_device *device,
			     double x, double y);

int
eis_device_event_pointer_abs(struct eis_device *device,
			     double x, double y);

int
eis_device_event_pointer_button(struct eis_device *device,
				uint32_t button, bool state);

int
eis_device_event_pointer_scroll(struct eis_device *device,
				double x, double y);

int
eis_device_event_pointer_scroll_discrete(struct eis_device *device,
					 int32_t x, int32_t y);
int
eis_device_event_pointer_scroll_stop(struct eis_device *device, bool x, bool y);

int
eis_device_event_pointer_scroll_cancel(struct eis_device *device, bool x, bool y);

int
eis_device_event_keyboard_key(struct eis_device *device,
			      uint32_t key, bool state);
int
eis_device_event_touch_down(struct eis_device *device, uint32_t touchid,
			    double x, double y);

int
eis_device_event_touch_motion(struct eis_device *device, uint32_t touchid,
			      double x, double y);

int
eis_device_event_touch_up(struct eis_device *device, uint32_t touchid);

void
eis_device_event_start_emulating(struct eis_device *device);

void
eis_device_event_stop_emulating(struct eis_device *device);

void
eis_device_closed_by_client(struct eis_device *device);

bool
eis_region_contains(struct eis_region *r, double x, double y);

struct eis_event *
eis_event_new_for_client(struct eis_client *client);

struct eis_event *
eis_event_new_for_seat(struct eis_seat *seat);

struct eis_event *
eis_event_new_for_device(struct eis_device *device);

struct eis *
eis_event_get_context(struct eis_event *event);

struct eis_event*
eis_event_ref(struct eis_event *event);

void
eis_queue_connect_event(struct eis_client *client);

void
eis_queue_disconnect_event(struct eis_client *client);

void
eis_queue_property_event(struct eis_client *client, const char *name,
			 const char *value, uint32_t permissions);

void
eis_queue_seat_bind_event(struct eis_seat *seat, uint32_t capabilities);

void
eis_queue_device_closed_event(struct eis_device *device);

void
eis_queue_frame_event(struct eis_device *device);

void
eis_queue_device_start_emulating_event(struct eis_device *device);

void
eis_queue_device_stop_emulating_event(struct eis_device *device);

void
eis_queue_pointer_rel_event(struct eis_device *device, double x, double y);

void
eis_queue_pointer_abs_event(struct eis_device *device,
			    double x, double y);

void
eis_queue_pointer_button_event(struct eis_device *device, uint32_t button,
			       bool is_press);

void
eis_queue_pointer_scroll_event(struct eis_device *device,
			       double x, double y);

void
eis_queue_pointer_scroll_discrete_event(struct eis_device *device,
					int32_t x, int32_t y);
void
eis_queue_pointer_scroll_stop_event(struct eis_device *device, bool x, bool y);

void
eis_queue_pointer_scroll_cancel_event(struct eis_device *device, bool x, bool y);

void
eis_queue_keyboard_key_event(struct eis_device *device, uint32_t key,
			     bool is_press);

void
eis_queue_touch_down_event(struct eis_device *device, uint32_t touchid,
			   double x, double y);

void
eis_queue_touch_motion_event(struct eis_device *device, uint32_t touchid,
			     double x, double y);

void
eis_queue_touch_up_event(struct eis_device *device, uint32_t touchid);

_printf_(6, 7) void
eis_log_msg(struct eis *eis,
	    enum eis_log_priority priority,
	    const char *file, int lineno, const char *func,
	    const char *format, ...);

_printf_(6, 0) void
eis_log_msg_va(struct eis *eis,
	       enum eis_log_priority priority,
	       const char *file, int lineno, const char *func,
	       const char *format,
	       va_list args);

#define log_debug(T_, ...) \
	eis_log_msg((T_), EIS_LOG_PRIORITY_DEBUG, __FILE__, __LINE__, __func__, __VA_ARGS__)
#define log_info(T_, ...) \
	eis_log_msg((T_), EIS_LOG_PRIORITY_INFO, __FILE__, __LINE__, __func__, __VA_ARGS__)
#define log_warn(T_, ...) \
	eis_log_msg((T_), EIS_LOG_PRIORITY_WARNING, __FILE__, __LINE__, __func__, __VA_ARGS__)
#define log_error(T_, ...) \
	eis_log_msg((T_), EIS_LOG_PRIORITY_ERROR, __FILE__, __LINE__, __func__, __VA_ARGS__)
#define log_bug(T_, ...) \
	eis_log_msg((T_), EIS_LOG_PRIORITY_ERROR, __FILE__, __LINE__, __func__, "🪳  libeis bug:  " __VA_ARGS__)
#define log_bug_client(T_, ...) \
	eis_log_msg((T_), EIS_LOG_PRIORITY_ERROR, __FILE__, __LINE__, __func__, "🪲  Bug: " __VA_ARGS__)
