/* SPDX-License-Identifier: MIT */
/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include "config.h"

#include "util-macros.h"
#include "util-mem.h"

#include "brei-shared.h"
#include "libei-private.h"

/* callbacks invoked during ei_proto_parse_message() */
struct ei_proto_interface {
	int (*connected)(struct ei *ei);
	int (*disconnected)(struct ei *ei);
	int (*seat_added)(struct ei *ei, uint32_t seatid,
			  const char *name, uint32_t capabilities);
	int (*seat_removed)(struct ei *ei, uint32_t seatid);
	int (*device_added)(struct ei *ei, uint32_t deviceid, uint32_t seatid,
			    const char *name, uint32_t capabilities, uint32_t type,
			    uint32_t width, uint32_t height);
	int (*device_removed)(struct ei *ei, uint32_t deviceid);
	int (*device_paused)(struct ei *ei, uint32_t deviceid);
	int (*device_resumed)(struct ei *ei, uint32_t deviceid);
	int (*device_done)(struct ei *ei, uint32_t deviceid);
	int (*device_region)(struct ei *ei, uint32_t deviceid,
			     uint32_t x, uint32_t y, uint32_t w, uint32_t h,
			     double scale);
	int (*device_keymap)(struct ei *ei, uint32_t deviceid,
			     enum ei_keymap_type keymap_type,
			     int keymap_fd,
			     size_t keymap_size);
	int (*keyboard_modifiers)(struct ei *ei, uint32_t deviceid,
				  uint32_t depressed, uint32_t latched,
				  uint32_t locked, uint32_t group);
	int (*property)(struct ei *ei,
			const char *name, const char *value,
			uint32_t permissions);

	/* events */
	int (*start_emulating)(struct ei *ei, uint32_t deviceid);
	int (*stop_emulating)(struct ei *ei, uint32_t deviceid);
	int (*rel)(struct ei *ei, uint32_t deviceid, double x, double y);
	int (*abs)(struct ei *ei, uint32_t deviceid, double x, double y);
	int (*button)(struct ei *ei, uint32_t deviceid, uint32_t button, bool is_press);
	int (*key)(struct ei *ei, uint32_t deviceid, uint32_t key, bool is_press);
	int (*scroll)(struct ei *ei, uint32_t deviceid, double x, double y);
	int (*scroll_stop)(struct ei *ei, uint32_t deviceid, bool x, bool y, bool is_cancel);
	int (*scroll_discrete)(struct ei *ei, uint32_t deviceid, int32_t x, int32_t y);
	int (*touch_down)(struct ei *ei, uint32_t deviceid,
			 uint32_t tid, double x, double y);
	int (*touch_motion)(struct ei *ei, uint32_t deviceid,
			    uint32_t tid, double x, double y);
	int (*touch_up)(struct ei *ei, uint32_t deviceid, uint32_t tid);
	int (*frame) (struct ei *ei, uint32_t deviceid);
};

struct ei_proto_requests {
	int (*connect)(struct ei *ei);
	int (*connect_done)(struct ei *ei);
	int (*disconnect)(struct ei *ei);
	int (*bind_seat)(struct ei_seat *seat, enum ei_device_capability cap);
	int (*unbind_seat)(struct ei_seat *seat, enum ei_device_capability cap);
	int (*close_device)(struct ei_device *device);
	int (*start_emulating)(struct ei_device *device);
	int (*stop_emulating)(struct ei_device *device);

	int (*rel)(struct ei_device *device, double x, double y);
	int (*abs)(struct ei_device *device, double x, double y);
	int (*button)(struct ei_device *device, uint32_t b, bool is_press);
	int (*key)(struct ei_device *device, uint32_t k, bool is_press);
	int (*scroll)(struct ei_device *device, double x, double y);
	int (*scroll_stop)(struct ei_device *device, bool x, bool y);
	int (*scroll_cancel)(struct ei_device *device, bool x, bool y);
	int (*scroll_discrete)(struct ei_device *device, int32_t x, int32_t y);
	int (*touch_down)(struct ei_device *device,
			 uint32_t tid, double x, double y);
	int (*touch_motion)(struct ei_device *device,
			    uint32_t tid, double x, double y);
	int (*touch_up)(struct ei_device *device, uint32_t tid);
	int (*frame)(struct ei_device *device);
	int (*property)(struct ei *ei,
			const char *name, const char *value,
			uint32_t permissions);
};

int
ei_proto_handle_message(struct ei *ei,
		       const struct ei_proto_interface *interface,
		       struct brei_message *message);

const struct ei_proto_requests *
ei_proto_get_requests(void);
