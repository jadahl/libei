/* SPDX-License-Identifier: MIT */
/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "config.h"

#include "util-macros.h"
#include "util-bits.h"
#include "util-strings.h"
#include "libeis-private.h"

static void
eis_seat_destroy(struct eis_seat *seat)
{
	struct eis_device *d;

	/* We expect those to have been removed already*/
	list_for_each(d, &seat->devices, link) {
		assert(!"device list not empty");
	}
	free(seat->name);
}

_public_
OBJECT_IMPLEMENT_REF(eis_seat);
_public_
OBJECT_IMPLEMENT_UNREF_CLEANUP(eis_seat);
static
OBJECT_IMPLEMENT_CREATE(eis_seat);
static
OBJECT_IMPLEMENT_PARENT(eis_seat, eis_client);
_public_
OBJECT_IMPLEMENT_GETTER(eis_seat, user_data, void *);
_public_
OBJECT_IMPLEMENT_SETTER(eis_seat, user_data, void *);
_public_
OBJECT_IMPLEMENT_GETTER(eis_seat, name, const char *);

_public_ struct eis_client *
eis_seat_get_client(struct eis_seat *seat)
{
	return eis_seat_parent(seat);
}

_public_ struct eis_seat *
eis_client_new_seat(struct eis_client *client, const char *name)
{
	static uint32_t seatid;
	struct eis_seat *seat = eis_seat_create(&client->object);

	/* we leave the lower bits to the deviceids */
	seat->id = ++seatid << 16;
	seat->state = EIS_SEAT_STATE_PENDING;
	seat->name = xstrdup(name);
	list_init(&seat->devices);
	/* seat is owned by caller until it's added */
	list_append(&client->seats_pending, &seat->link);

	return seat;
}

_public_ void
eis_seat_add(struct eis_seat *seat)
{
	struct eis_client *client = eis_seat_get_client(seat);

	switch (seat->state) {
	case EIS_SEAT_STATE_PENDING:
		break;
	case EIS_SEAT_STATE_ADDED:
	case EIS_SEAT_STATE_BOUND:
	case EIS_SEAT_STATE_REMOVED:
	case EIS_SEAT_STATE_REMOVED_INTERNALLY:
	case EIS_SEAT_STATE_DEAD:
		log_bug_client(eis_client_get_context(client),
			       "%s: seat already added/removed/dead\n", __func__);
		return;
	}

	seat->state = EIS_SEAT_STATE_ADDED;
	eis_client_add_seat(client, seat);
}

void
eis_seat_bind(struct eis_seat *seat, uint32_t caps)
{
	struct eis_client *client = eis_seat_get_client(seat);

	switch (seat->state) {
	case EIS_SEAT_STATE_ADDED:
	case EIS_SEAT_STATE_BOUND:
		break;
	case EIS_SEAT_STATE_PENDING:
	case EIS_SEAT_STATE_REMOVED:
	case EIS_SEAT_STATE_REMOVED_INTERNALLY:
	case EIS_SEAT_STATE_DEAD:
		log_bug_client(eis_client_get_context(client),
			       "%s: seat cannot be bound\n", __func__);
		return;
	}

	caps &= seat->capabilities_mask;

	seat->state = EIS_SEAT_STATE_BOUND;
	eis_queue_seat_bind_event(seat, caps);
}

void
eis_seat_drop(struct eis_seat *seat)
{
	if (seat->state == EIS_SEAT_STATE_BOUND)
		eis_seat_bind(seat, 0);

	struct eis_device *d;
	list_for_each_safe(d, &seat->devices, link) {
		eis_device_remove(d);
	}

	eis_client_remove_seat(eis_seat_get_client(seat), seat);
	list_remove(&seat->link);
	seat->state = EIS_SEAT_STATE_REMOVED_INTERNALLY;
	eis_seat_unref(seat);
}

_public_ void
eis_seat_remove(struct eis_seat *seat)
{
	struct eis_client *client = eis_seat_get_client(seat);
	_unref_(eis_seat) *s = eis_seat_ref(seat);

	switch (seat->state) {
	case EIS_SEAT_STATE_PENDING:
	case EIS_SEAT_STATE_ADDED:
	case EIS_SEAT_STATE_BOUND:
		eis_seat_drop(s);
		s->state = EIS_SEAT_STATE_REMOVED;
		break;
	case EIS_SEAT_STATE_REMOVED_INTERNALLY:
		s->state = EIS_SEAT_STATE_REMOVED;
		break;
	case EIS_SEAT_STATE_REMOVED:
	case EIS_SEAT_STATE_DEAD:
		log_bug_client(eis_client_get_context(client),
			       "%s: seat already removed\n", __func__);
		return;
	}

}

_public_ void
eis_seat_configure_capability(struct eis_seat *seat,
			      enum eis_device_capability cap)
{
	if (seat->state != EIS_SEAT_STATE_PENDING)
		return;

	switch (cap) {
	case EIS_DEVICE_CAP_POINTER:
	case EIS_DEVICE_CAP_POINTER_ABSOLUTE:
	case EIS_DEVICE_CAP_KEYBOARD:
	case EIS_DEVICE_CAP_TOUCH:
		flag_set(seat->capabilities_mask, cap);
		break;
	}
}

_public_ bool
eis_seat_has_capability(struct eis_seat *seat,
			enum eis_device_capability cap)
{
	return flag_is_set(seat->capabilities_mask, cap);
}
