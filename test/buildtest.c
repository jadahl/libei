/* SPDX-License-Identifier: MIT */
#if INCLUDE_LIBEI
#include <libei.h>
#endif

#if INCLUDE_LIBEIS
#include <libeis.h>
#endif

#if INCLUDE_LIBREIS
#include <libreis.h>
#endif

int main(int argc, char **argv) {
	return 0;
}
